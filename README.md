# Money Transfer REST API


This is a REST API that allows users to create bank accounts and make money
transfers to other bank accounts.

The API is voluntarily kept minimalist.
Advanced user management and currency support for instance were left out.

The focus was given on a robust and tested implementation of the core method which is: 
the ability to transfer funds from accounts to others in a multi threaded environment.

#### Note on the tests

Besides Unit Testing, Functional Testing is separated in two files:

 
    SingleThreadedFunctionalTest.groovy
    
    MultiThreadedFunctionalTest.groovy


The first one details every supported use case of the API.

The second one focuses on synchronization and deadlocks. It tries to prove that
many concurrent transactions on a very small number of accounts does not lock the system
down (for instance: 100 threads doing 100 transactions each on only 2 accounts).

Every test case is built on the on-memory data store, no mocking was found necessary in
that context.
    
#### About the database and the synchronisation method

In order for the data to be consistent and for the service architecture to be close to
a system that can scale out,
an in memory SQL database was used rather than a concurrent key value store.

The synchronization mechanism is not implemented in the java code, but delegated to the 
database itself so that multiple instances of the service could be synchronized together
ensuring data consistency.

#### About the SQL Schema

- The system uses UUID as primary keys to avoid adjacent ids for bank accounts.
- The system stores every successful transactions to be ale to check its consistency.
- The money is modeled with BigDecimals to enable precise operations. 


## Installation from sources

In order to build the executable jar of the service, you need to have
maven 3 and a JDK 11 installed.

Download the sources using:

    git clone https://bitbucket.org/wiesenfe/money-transfer-rest.git
    
Go to the source directory.
Configure the application editing the file:

    src/main/resources/system.properties
    
       
Then build with:

    mvn clean package
    
To run the application:

    java -jar target/money-transfer-api-jar-with-dependencies.jar
   

## API documentation

### Identification

There is not authentication mechanism in this REST API, but you need to identify
yourself by a user id.

The user id should be added in the header of all your requests.
It should be added in the header field: uid and it should be a positive integer.

For instance:

    uid:123
    
### Errors

The API uses HTTP error codes like so:

- 500 If there is a server error
- 400 If the input is incorrect
- 401 If an action is not authorized
- 404 If a route is not found

The API will usually give details about the cause of the error:

``` 
{
    "errorMessage": "The user id is either missing or malformed",
    "errorCode": "INVALID_USER_ID"
}
``` 

### Accounts management

There is only one currency for every bank account, so it does not appear in the API.
Bank accounts have three attributes:

- The bank account unique id
- The balance of the account
- The owner's uid


#### 1. Create a bank account

Creates a bank account in the system with a given initial balance.
You will be designated as the owner of the created account.

**Request:**
    
    PUT /account/${initial balance}
    
**Response:**

```
{
    "id":"25e3b9bc-56c2-456f-9f36-9af0c1dab95e",
    "balance": ${the initial balance on your account},
    "owner": ${your user id}
}
```

#### 2. Retrieve details of your account

Loads the details of a given bank account.
You must be the owner of the account to access its details.

**Request:**

    GET /account/${your account id}
    

**Response:**

```
{
    "id": "fea45116-91db-449c-8c82-4deaf59a08b3",
    "balance": ${the current balance on your account},
    "owner": ${your user id}
}
```

### Transaction management

The system lets you move funds from accounts that you own to
any other existing accounts.
You can also review every transaction that were made to and from
a given account (that you own).

#### 1. Make a money transfer transaction

In order to send funds from one of your accounts to an other one:

**Request:**

    POST /transaction/${from}/${to}/${amount}
    

Where 

- ${from} is the account id to debit
- ${to} the account id 
to credit 
- ${amount} the amount of money you are transferring.


**Response:**

```
{
    "transactionId": "6289e096-4484-4f6c-a2f1-94a3deeb84e6",
    "accountIdToCredit": "82b027b8-c963-43a1-a617-f402b53a0d19",
    "accountIdToDebit": "c5eb1871-f3ea-4c6f-8027-1d3c5445bf71",
    "amount": 10,
    "status": "SUCCESS",
    "completionUtcTimestamp": 1553435672642,
    "success": true
}
```

#### 2. Retrieve transactions from and to one of your accounts

**Request:**

    GET /transaction/${direction}/${your account id}
    
Where

- ${direction} can be either **from** or **to**
- ${your account id} the account id you are checking


**Response:**

```
[
    {
        "transactionId": "6289e096-4484-4f6c-a2f1-94a3deeb84e6",
        "accountIdToCredit": "82b027b8-c963-43a1-a617-f402b53a0d19",
        "accountIdToDebit": "c5eb1871-f3ea-4c6f-8027-1d3c5445bf71",
        "amount": 10,
        "status": "SUCCESS",
        "completionUtcTimestamp": 1553435672642,
        "success": true
    },
    {
        "transactionId": "fe603698-db50-4587-8b06-8eb5a0defd86",
        "accountIdToCredit": "82b027b8-c963-43a1-a617-f402b53a0d19",
        "accountIdToDebit": "c5eb1871-f3ea-4c6f-8027-1d3c5445bf71",
        "amount": 10,
        "status": "SUCCESS",
        "completionUtcTimestamp": 1553436993009,
        "success": true
    }
]
```

Note that only the effective transactions appear.
Meaning that only transactions with success = true will be in
the response.


### Admin methods

The status of the API can be retrieved here:

    GET /status
    
If the API is up:

```
{
    "status": "up"
}

```







     