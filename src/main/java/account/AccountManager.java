package account;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface AccountManager {

    /**
     * Creates a new account in the system with {@param initialBalance}
     * The account ID will be a randomly generated UID
     *
     * @return The created account
     */
    @NotNull
    Account createAccount(@Nullable String initialBalanceStr, int userId);

    /**
     * Loads the accounts identified by {@param accountId}
     *
     * @return The loaded account
     * @throws reporting.AccountManagementException if the account was not found
     */
    @NotNull
    Account loadAccount(@Nullable String accountIdStr, int userId);
}
