package account;

import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public final class AccountHandler extends BeanHandler<Account> {

    public AccountHandler() {
        super(Account.class);
    }

    @Override
    public Account handle(ResultSet rs) throws SQLException {
        return rs.first() ? new Account(
                rs.getObject("id", UUID.class),
                rs.getBigDecimal("balance"),
                rs.getInt("owner")
        ) : null;
    }
}
