package account;

import datastore.SQLDataStore;
import datastore.SQLDataStoreInMemoryH2;
import org.apache.commons.dbutils.DbUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reporting.AccountManagementException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import static datastore.SQLDataStore.ACCOUNT_TABLE;
import static datastore.SQLDataStore.runner;
import static reporting.TransactionStatus.*;

public enum AccountManagerImpl implements AccountManager {
    INSTANCE(SQLDataStoreInMemoryH2.INSTANCE);

    @NotNull
    private final Logger logger = LoggerFactory.getLogger(AccountManagerImpl.class);

    @NotNull
    private final SQLDataStore dataStoreManager;

    AccountManagerImpl(@NotNull SQLDataStore dataStoreManager) {
        this.dataStoreManager = dataStoreManager;
    }

    @Override
    public @NotNull Account createAccount(@Nullable String initialBalanceStr, int userId) {
        var accountUUID = UUID.randomUUID();
        logger.info("Creating a new account with balance = {} and id = {} for user {}",
                initialBalanceStr, accountUUID, userId);
        var initialBalance = AccountItemsFactory.buildBalanceFromString(initialBalanceStr);

        Connection con = null;
        try {
            con = dataStoreManager.getConnection();
            runner.execute(con, "INSERT INTO " + ACCOUNT_TABLE + " VALUES ( ? , ?, ? )", accountUUID, initialBalance, userId);
            con.commit();
        } catch (SQLException e1) {
            try {
                if (con != null) {
                    con.rollback();
                }
            } catch (SQLException e2) {
                logger.error("Unable to rollback failed account creation", e2);
            }
            throw new AccountManagementException(ERROR, e1);

        } finally {
            DbUtils.closeQuietly(con);
        }

        return new Account(accountUUID, initialBalance, userId);
    }

    @Override
    public @NotNull Account loadAccount(@Nullable String accountIdStr, int userId) {
        logger.debug("User {} is loading info for account {}", userId, accountIdStr);
        // Specify read only so we can access the data while locked for
        // transaction.
        var account = dataStoreManager.readOnlyQuery(
                "SELECT * FROM " + ACCOUNT_TABLE + " WHERE id = ? FOR READ ONLY",
                new AccountHandler(),
                AccountItemsFactory.buildAccountIdFromString(accountIdStr));
        if (account == null) {
            throw new AccountManagementException(ACCOUNT_IS_UNKNOWN);
        }
        if (!account.canBeAccessedBy(userId)) {
            throw new AccountManagementException(USER_NOT_OWNER);
        }
        return account;
    }
}
