package account;

import org.jetbrains.annotations.NotNull;
import spark.utils.Assert;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public final class Account {

    @NotNull
    private final UUID id;

    @NotNull
    private final BigDecimal balance;

    private final int owner;

    Account(@NotNull UUID id, @NotNull BigDecimal balance, int owner) {
        this.id = id;
        this.balance = balance;
        this.owner = owner;
    }

    public boolean canBeAccessedBy(int userId) {
        return owner == userId;
    }

    public Account retrieveFunds(BigDecimal amount) {
        Assert.isTrue(amount.compareTo(BigDecimal.ZERO) >= 0, "The amount must be positive");
        return new Account(this.id, this.balance.subtract(amount), this.owner);
    }

    public Account depositFunds(BigDecimal amount) {
        Assert.isTrue(amount.compareTo(BigDecimal.ZERO) >= 0, "The amount must be positive");
        return new Account(this.id, this.balance.add(amount), this.owner);
    }

    public boolean hasSufficientFund(@NotNull BigDecimal amountToDebit) {
        Assert.isTrue(amountToDebit.compareTo(BigDecimal.ZERO) >= 0, "The amountToDebit must be positive");
        return this.balance.compareTo(amountToDebit) >= 0;
    }

    @NotNull
    public BigDecimal getBalance() {
        return balance;
    }

    @NotNull
    public UUID getId() {
        return id;
    }

    public int getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id.equals(account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                ", owner=" + owner +
                '}';
    }
}
