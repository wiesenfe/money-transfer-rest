package account;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import reporting.AccountManagementException;

import java.math.BigDecimal;
import java.util.UUID;

import static reporting.TransactionStatus.*;

public final class AccountItemsFactory {

    /**
     * Builds a UUID from a string {@param str}
     *
     * @return the built UUID, never null
     * @throws AccountManagementException if the UUID is malformed
     */
    @NotNull
    public static UUID buildAccountIdFromString(@Nullable String str) {
        if (str == null) {
            throw new AccountManagementException(INVALID_UUID);
        }
        UUID accountId;
        try {
            accountId = UUID.fromString(str);
        } catch (IllegalArgumentException e) {
            throw new AccountManagementException(INVALID_UUID, e);
        }
        return accountId;
    }

    /**
     * Builds a BigDecimal from a string {@param str}
     *
     * @return the built UUID, never null
     * @throws AccountManagementException if the balance is malformed
     */
    @NotNull
    public static BigDecimal buildBalanceFromString(@Nullable String str) {
        if (str == null) {
            throw new AccountManagementException(ACCOUNT_WITH_MALFORMED_INITIAL_BALANCE);
        }

        BigDecimal parsedBalance;
        try {
            parsedBalance = new BigDecimal(str);
        } catch (NumberFormatException e) {
            throw new AccountManagementException(ACCOUNT_WITH_MALFORMED_INITIAL_BALANCE, e);
        }

        if (parsedBalance.compareTo(BigDecimal.ZERO) < 0) {
            throw new AccountManagementException(AMOUNT_IS_NEGATIVE);
        }

        return parsedBalance;
    }
}
