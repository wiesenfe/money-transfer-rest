package reporting;

import org.jetbrains.annotations.NotNull;

public final class AccountManagementException extends OperationException {

    public AccountManagementException(@NotNull TransactionStatus status) {
        super(status);
    }

    public AccountManagementException(TransactionStatus status, Throwable cause) {
        super(status, cause);
    }
}
