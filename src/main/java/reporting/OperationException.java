package reporting;

import org.jetbrains.annotations.NotNull;

public abstract class OperationException extends RuntimeException {


    @NotNull
    private final TransactionStatus status;

    OperationException(TransactionStatus status, Throwable cause) {
        super(status.getMessage(), cause);
        this.status = status;
    }

    OperationException(TransactionStatus status) {
        super(status.getMessage());
        this.status = status;
    }

    @NotNull
    public TransactionStatus getStatus() {
        return status;
    }

    public int getHttpCode() {
        return status.getHttpCode();
    }
}
