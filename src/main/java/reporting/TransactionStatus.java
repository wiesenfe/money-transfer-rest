package reporting;

import org.jetbrains.annotations.NotNull;

public enum TransactionStatus {

    // Transfer related status
    STARTED("The transaction has started", 200),
    SUCCESS("The transaction has been successful", 200),
    NOT_ENOUGH_FUND("The transaction could not be completed due to insufficient funds", 400),
    UNKNOWN_BENEFICIARY("The beneficiary account was not found", 400),
    UNKNOWN_SOURCE("The source account was not found", 400),
    NEGATIVE_AMOUNT_IN_TRANSFER("You cannot transfer a negative amount", 400),
    NO_FUND_TO_TRANSFER("You can't transfer 0 unit from one account to another", 400),
    SAME_ACCOUNT_TRANSFER("The source and the beneficiary are the same", 400),

    // Account related status
    INVALID_UUID("The account id you provided is malformed", 400),
    ACCOUNT_WITH_MALFORMED_INITIAL_BALANCE("The initial balance that you provided is malformed", 400),
    AMOUNT_IS_NEGATIVE("No account can be created with an initial negative balance", 400),
    ACCOUNT_IS_UNKNOWN("The account you are looking for is not known", 400),

    // User permission
    INVALID_USER_ID("The user id is either missing or malformed", 401),
    USER_NOT_OWNER("The user id you provided does not own the request item", 401),

    // Generic error
    ERROR("A system error occurred", 500);

    TransactionStatus(@NotNull String message, int errorCode) {
        this.httpCode = errorCode;
        this.message = message;
    }

    @NotNull
    private final String message;

    private final int httpCode;

    @NotNull
    public String getMessage() {
        return message;
    }

    @SuppressWarnings("unused")
    public int getHttpCode() {
        return httpCode;
    }
}
