package reporting;

import org.jetbrains.annotations.NotNull;

public final class TransactionException extends OperationException {

    public TransactionException(@NotNull TransactionStatus status) {
        super(status);
    }
}
