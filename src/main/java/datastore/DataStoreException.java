package datastore;

final class DataStoreException extends RuntimeException {

    DataStoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
