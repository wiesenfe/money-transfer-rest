package datastore;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.h2.jdbcx.JdbcConnectionPool;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Implementation for testing purpose only.
 * <p>
 * When constructing this singleton, the SQL Schema will be
 * initialized.
 */
public enum SQLDataStoreInMemoryH2 implements SQLDataStore {
    INSTANCE;

    @NotNull
    private final Logger logger = LoggerFactory.getLogger(SQLDataStoreInMemoryH2.class);


    @NotNull
    private final JdbcConnectionPool connectionPool;

    SQLDataStoreInMemoryH2() {
        connectionPool = JdbcConnectionPool.create(getConnectionUrl(), "", "");
        initSchema();
    }

    private void initSchema() {
        logger.info("SQL Data store: init SQL schema");

        var createAccountsTableQuery = "CREATE TABLE " + ACCOUNT_TABLE + " (id uuid primary key, " +
                "balance decimal, owner int)";
        var createTransactionsTableQuery = "CREATE TABLE " + TX_TABLE + " (id uuid primary key, credited_account uuid, " +
                "debited_account uuid, amount decimal, utc_timestamp long)";

        Connection connection = null;
        try {
            var runner = new QueryRunner();
            connection = this.getConnection();
            runner.execute(connection, createAccountsTableQuery);
            runner.execute(connection, createTransactionsTableQuery);

        } catch (SQLException e) {
            throw new DataStoreException("Impossible to init the SQL schema", e);
        } finally {
            DbUtils.closeQuietly(connection);
        }
    }

    @Override
    public @NotNull String getConnectionUrl() {
        return "jdbc:h2:mem:test;MV_STORE=TRUE;DB_CLOSE_DELAY=-1";
    }

    @Override
    public @NotNull Connection getConnection() throws SQLException {
        logger.debug("Getting a new connection from the data store connection pool");
        var connection = connectionPool.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    @Override
    public <T> T readOnlyQuery(String sql, ResultSetHandler<T> handler, Object... params) {
        logger.debug("A new read only query (for an object) is being made to the data store: {}", sql);
        Connection connection = null;
        try {
            connection = getConnection();
            return runner.query(connection, sql, handler, params);
        } catch (SQLException e) {
            throw new DataStoreException("Could not execute " + sql, e);
        } finally {
            DbUtils.closeQuietly(connection);
        }
    }

    @Override
    public long readOnlyQuery(String sql, Object... params) {
        logger.debug("A new read only query (for a number) is being made to the data store: {}", sql);
        Connection connection = null;
        try {
            connection = getConnection();
            return runner.query(connection, sql, new ScalarHandler<>(), params);
        } catch (SQLException e) {
            throw new DataStoreException("Could not execute " + sql, e);
        } finally {
            DbUtils.closeQuietly(connection);
        }
    }

    @Override
    public void setMaxConnections(int max) {
        logger.debug("Max connection in the data store thread pool set to: {}", max);
        this.connectionPool.setMaxConnections(max);
    }
}
