package datastore;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;

public interface SQLDataStore {

    String ACCOUNT_TABLE = "T_ACCOUNT";
    String TX_TABLE = "T_TRANSACTION";
    QueryRunner runner = new QueryRunner();

    @NotNull
    String getConnectionUrl();

    @NotNull
    Connection getConnection() throws SQLException;

    <T> T readOnlyQuery(String sql, ResultSetHandler<T> handler, Object... params);

    long readOnlyQuery(String sql,  Object... params);


    void setMaxConnections(int max);
}
