package transaction;

import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.jetbrains.annotations.NotNull;
import reporting.TransactionStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TransactionListHandler extends BeanListHandler<Transaction> {

    TransactionListHandler() {
        super(Transaction.class);
    }

    @Override
    public @NotNull List<Transaction> handle(ResultSet rs) throws SQLException {
        var result = new ArrayList<Transaction>();
        while (rs.next()) {
            result.add(new Transaction(
                    rs.getObject("id", UUID.class),
                    rs.getLong("utc_timestamp"),
                    rs.getObject("credited_account", UUID.class),
                    rs.getObject("debited_account", UUID.class),
                    rs.getBigDecimal("amount"),
                    TransactionStatus.SUCCESS));
        }
        return result;
    }
}
