package transaction;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

public interface TxManager {

    /**
     * (Thread)Safely executes and registers the given {@param transactionToExecute}
     * for the given {@param userId} who must own the debited account
     *
     * @return The resulting transaction with a resulting status
     */
    @NotNull
    Transaction executeTransaction(@NotNull Transaction transactionToExecute, int userId);


    /**
     * Loads the list of transactions from {@param debitedAccountId}
     * Meaning: all the transactions that took money out of the specified account
     * The {@param userId} must own the {@param debitedAccountId}
     *
     * @return the list of matching transactions, can be empty.
     */
    @NotNull
    List<Transaction> loadTransactionsFrom(@NotNull UUID debitedAccountId, int userId);

    /**
     * Loads the list of transactions to {@param creditedAccountId}
     * Meaning: all the transactions that put money on the specified account
     * The {@param userId} must own the {@param creditedAccountId}
     *
     * @return the list of matching transactions, can be empty.
     */
    @NotNull
    List<Transaction> loadTransactionsTo(@NotNull UUID creditedAccountId, int userId);

    /**
     * Simply counts the transactions that were done so far
     *
     * @return The number of successful transactions in the system
     */
    long countTransactions();
}
