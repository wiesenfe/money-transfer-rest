package transaction;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

final class UnknownAccountException extends RuntimeException {

    @NotNull
    private final UUID unknownAccountId;

    UnknownAccountException(@NotNull UUID unknownAccountId) {
        super();
        this.unknownAccountId = unknownAccountId;
    }

    @NotNull
    UUID getUnknownAccountId() {
        return unknownAccountId;
    }
}
