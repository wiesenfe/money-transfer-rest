package transaction;

import account.AccountItemsFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import reporting.TransactionStatus;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

public final class TransactionFactory {

    @NotNull
    public static Transaction initTransactionFromUserInput(
            @Nullable String accountToCredit,
            @Nullable String accountToDebit,
            @Nullable String amount) {
        return initTransaction(
                AccountItemsFactory.buildAccountIdFromString(accountToCredit),
                AccountItemsFactory.buildAccountIdFromString(accountToDebit),
                AccountItemsFactory.buildBalanceFromString(amount)
        );
    }

    @NotNull
    static Transaction initTransaction(
            @NotNull UUID accountToCredit, @NotNull UUID accountToDebit,
            @NotNull BigDecimal amount) {
        return new Transaction(
                UUID.randomUUID(),
                0L,
                accountToCredit,
                accountToDebit,
                amount,
                TransactionStatus.STARTED
        );
    }

    @NotNull
    static Transaction completeTransaction(@NotNull Transaction transaction, @NotNull TransactionStatus status) {
        return new Transaction(
                transaction.getTransactionId(),
                Instant.now().toEpochMilli(),
                transaction.getAccountIdToCredit(),
                transaction.getAccountIdToDebit(),
                transaction.getAmount(),
                status
        );
    }
}
