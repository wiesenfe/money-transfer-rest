package transaction;

import org.jetbrains.annotations.NotNull;
import reporting.TransactionStatus;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;


public final class Transaction {

    @NotNull
    private final UUID transactionId;

    @NotNull
    private final UUID accountIdToCredit;

    @NotNull
    private final UUID accountIdToDebit;

    @NotNull
    private final BigDecimal amount;

    @NotNull
    private final TransactionStatus status;

    @NotNull
    private final Long completionUtcTimestamp;

    private final boolean success;

    Transaction(
            @NotNull UUID transactionId, @NotNull Long completionUtcTimestamp,
            @NotNull UUID accountIdToCredit, @NotNull UUID accountIdToDebit,
            @NotNull BigDecimal amount, @NotNull TransactionStatus status) {
        this.transactionId = transactionId;
        this.accountIdToCredit = accountIdToCredit;
        this.accountIdToDebit = accountIdToDebit;
        this.amount = amount;
        this.status = status;
        this.completionUtcTimestamp = completionUtcTimestamp;
        this.success = status == TransactionStatus.SUCCESS;
    }

    boolean accountsAreIdentical() {
        return Objects.equals(this.accountIdToCredit, this.accountIdToDebit);
    }

    boolean isFirstAccountToDebit() {
        return Objects.equals(getAccountIdToDebit(), getFirstAccountId());
    }

    @NotNull UUID getFirstAccountId() {
        return accountIdToCredit.compareTo(accountIdToDebit) < 0 ? accountIdToCredit : accountIdToDebit;
    }

    @NotNull UUID getSecondAccountId() {
        return accountIdToCredit.compareTo(accountIdToDebit) < 0 ? accountIdToDebit : accountIdToCredit;
    }

    @NotNull UUID getAccountIdToCredit() {
        return accountIdToCredit;
    }

    @NotNull
    UUID getAccountIdToDebit() {
        return accountIdToDebit;
    }

    @NotNull
    BigDecimal getAmount() {
        return amount;
    }

    @NotNull
    TransactionStatus getStatus() {
        return status;
    }

    @NotNull
    UUID getTransactionId() {
        return transactionId;
    }

    @NotNull
    Long getCompletionUtcTimestamp() {
        return completionUtcTimestamp;
    }

    @SuppressWarnings("unused")
    boolean isSuccess() {
        return success;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId=" + transactionId +
                ", accountIdToCredit=" + accountIdToCredit +
                ", accountIdToDebit=" + accountIdToDebit +
                ", amount=" + amount +
                ", status=" + status +
                ", completionUtcTimestamp=" + completionUtcTimestamp +
                '}';
    }
}
