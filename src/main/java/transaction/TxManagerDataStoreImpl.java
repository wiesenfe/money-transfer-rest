package transaction;

import account.Account;
import account.AccountHandler;
import account.AccountManager;
import account.AccountManagerImpl;
import datastore.SQLDataStore;
import datastore.SQLDataStoreInMemoryH2;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reporting.TransactionException;
import reporting.TransactionStatus;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static datastore.SQLDataStore.*;
import static reporting.TransactionStatus.*;

public enum TxManagerDataStoreImpl implements TxManager {
    INSTANCE(SQLDataStoreInMemoryH2.INSTANCE, AccountManagerImpl.INSTANCE);

    @NotNull
    private final Logger logger = LoggerFactory.getLogger(TxManagerDataStoreImpl.class);

    @NotNull
    private final AccountHandler accountHandler;

    @NotNull
    private final TransactionListHandler txListHandler;

    @NotNull
    private final SQLDataStore dataStoreManager;

    @NotNull
    private final AccountManager accountManager;

    TxManagerDataStoreImpl(@NotNull SQLDataStore dataStore, @NotNull AccountManager accountManager) {
        this.dataStoreManager = dataStore;
        this.accountHandler = new AccountHandler();
        this.accountManager = accountManager;
        txListHandler = new TransactionListHandler();
    }

    @Override
    @NotNull
    public Transaction executeTransaction(@NotNull Transaction tx, int userId) {
        logger.info("Working on a new transaction {} on behalf of user {}", tx, userId);
        Connection connection = null;
        try {
            preliminaryChecks(tx);

            // Perform transaction on locked accounts
            connection = dataStoreManager.getConnection();
            var accounts = loadAndLockAccounts(tx, connection, userId);

            logger.debug("The transaction {} was processed correctly", tx);
            return performTransactionAndCommit(accounts.left, accounts.right, tx, connection);

        } catch (SQLException e) {
            logger.warn("SQL Exception during transaction execution", e);
            return finalizeWithErrorAndRollback(ERROR, tx, connection);

        } catch (TransactionException e) {
            logger.warn("A Domain specific transaction was raised: " + e.getStatus());
            return finalizeWithErrorAndRollback(e.getStatus(), tx, connection);

        } catch (UnknownAccountException e) {
            logger.warn("An account was not found: " + e.getUnknownAccountId());
            var status = Objects.equals(e.getUnknownAccountId(), tx.getAccountIdToCredit())
                    ? UNKNOWN_BENEFICIARY
                    : UNKNOWN_SOURCE;
            return finalizeWithErrorAndRollback(status, tx, connection);

        } catch (Throwable t) {
            // In case a runtime exception is thrown somewhere, properly cleanup the mess
            logger.warn("Unexpected exception during transaction", t);
            return finalizeWithErrorAndRollback(ERROR, tx, connection);

        } finally {
            DbUtils.closeQuietly(connection);
        }
    }

    @Override
    public @NotNull List<Transaction> loadTransactionsFrom(@NotNull UUID debitedAccountId, int userId) {
        logger.info("User {} is loading transaction from account {}", userId, debitedAccountId);
        accountManager.loadAccount(debitedAccountId.toString(), userId); // Checks permission
        var loadQuery = "SELECT id, credited_account, debited_account, amount, utc_timestamp " +
                "FROM " + TX_TABLE + " WHERE debited_account = ?";
        return dataStoreManager.readOnlyQuery(loadQuery, txListHandler, debitedAccountId);
    }

    @Override
    public @NotNull List<Transaction> loadTransactionsTo(@NotNull UUID creditedAccountId, int userId) {
        logger.info("User {} is loading transaction to account {}", userId, creditedAccountId);
        accountManager.loadAccount(creditedAccountId.toString(), userId); // Checks permission
        var loadQuery = "SELECT id, credited_account, debited_account, amount, utc_timestamp " +
                "FROM " + TX_TABLE + " WHERE credited_account = ?";
        return dataStoreManager.readOnlyQuery(loadQuery, txListHandler, creditedAccountId);
    }

    @Override
    public long countTransactions() {
        return dataStoreManager.readOnlyQuery("SELECT COUNT (id) FROM " + TX_TABLE);
    }


    /**
     * Checks {@param tx} for inconsistencies
     * Will throw a runtime TransactionException if we find an issue
     */
    private void preliminaryChecks(@NotNull Transaction tx) {
        logger.debug("Running checks on transaction {} before locking the accounts", tx);
        if (tx.accountsAreIdentical()) {
            throw new TransactionException(SAME_ACCOUNT_TRANSFER);
        }
        if (tx.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new TransactionException(NEGATIVE_AMOUNT_IN_TRANSFER);
        }
        if (tx.getAmount().compareTo(BigDecimal.ZERO) == 0) {
            throw new TransactionException(NO_FUND_TO_TRANSFER);
        }
    }

    /**
     * Loads and locks the accounts for the {@param tx} using {@param connection}.
     * Once loaded, we assign them to the members of the pair.
     * IMPORTANT: The accounts are loaded/locked in natural order to avoid deadlocks.
     *
     * @return a pair of accounts, in that order: account to debit, account to credit
     */
    @NotNull
    private ImmutablePair<Account, Account> loadAndLockAccounts(
            @NotNull Transaction tx,
            @NotNull Connection connection,
            int userId) throws SQLException {
        logger.debug("Locking accounts for transaction in data store");

        // Use ordering on UUID to lock
        var account1 = loadAndLockAccount(tx.getFirstAccountId(), connection);
        var account2 = loadAndLockAccount(tx.getSecondAccountId(), connection);

        var accountToDebit = tx.isFirstAccountToDebit() ? account1 : account2;
        var accountToCredit = tx.isFirstAccountToDebit() ? account2 : account1;

        if (!accountToDebit.canBeAccessedBy(userId)) {
            throw new TransactionException(USER_NOT_OWNER);
        }

        logger.debug("Both accounts were locked: {} -> {}", accountToDebit, accountToCredit);
        return new ImmutablePair<>(accountToDebit, accountToCredit);
    }

    /**
     * Builds a finalized transaction with the given {@param status} from the initial {@param tx}
     * Ends the SQL Transaction with a rollback.
     *
     * @param connection is required to perform the rollback.
     * @return The finalized transaction
     */
    @NotNull
    private Transaction finalizeWithErrorAndRollback(
            @NotNull TransactionStatus status,
            @NotNull Transaction tx,
            @Nullable Connection connection) {
        logger.debug("The transaction {} is in error with status {}: Roll back", tx, status);
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Unable to rollback transaction, the system might be in an inconsistent state", e);
            }
        }
        return TransactionFactory.completeTransaction(tx, status);
    }

    /**
     * Performs the transaction in the data store by moving the funds
     * from one account {@param accountToDebit} to the other {@param accountToCredit}.
     * Builds a finalized with status = SUCCESS, from the initial {@param tx}.
     * Also log the transaction if successful into the TX table.
     *
     * @param connection Needed to perform updates and commit
     * @return The finalized and successful transaction.
     * @throws SQLException in case any operation against the Data Store fails
     */
    @NotNull
    private Transaction performTransactionAndCommit(
            @NotNull Account accountToDebit,
            @NotNull Account accountToCredit,
            @NotNull Transaction tx,
            @NotNull Connection connection) throws SQLException {

        logger.debug("Now moving {} units from {} to {}", tx.getAmount(), accountToDebit, accountToCredit);
        var updateAccountQuery = "UPDATE " + ACCOUNT_TABLE + " SET balance = ? WHERE id = ?";

        if (!accountToDebit.hasSufficientFund(tx.getAmount())) {
            throw new TransactionException(NOT_ENOUGH_FUND);
        }

        var debitedAccount = accountToDebit.retrieveFunds(tx.getAmount());
        var creditedAccount = accountToCredit.depositFunds(tx.getAmount());

        runner.execute(connection, updateAccountQuery, debitedAccount.getBalance(), debitedAccount.getId());
        runner.execute(connection, updateAccountQuery, creditedAccount.getBalance(), creditedAccount.getId());

        var completeTx = TransactionFactory.completeTransaction(tx, SUCCESS);
        persistSuccessfulTransaction(completeTx, connection);

        logger.debug("The transaction is successful, commit it to the DB");
        connection.commit();
        return completeTx;
    }

    /**
     * Writes the finalized and successful {@param tx} using {@param connection}
     *
     * @throws SQLException In case the operation against the data store fails
     */
    private void persistSuccessfulTransaction(@NotNull Transaction tx,
                                              @NotNull Connection connection) throws SQLException {
        var insertTransactionQuery = "INSERT INTO " + TX_TABLE + " VALUES ( ?, ?, ?, ?, ? )";
        logger.debug("Inserting a new successful transaction: {}", tx);
        runner.execute(connection, insertTransactionQuery,
                tx.getTransactionId(),
                tx.getAccountIdToCredit(),
                tx.getAccountIdToDebit(),
                tx.getAmount(),
                tx.getCompletionUtcTimestamp());
    }

    /**
     * Loads the accounts identified by {@param accountId} using {@param connection}
     * The loading sets a lock on the read row ONLY as long as the transaction is ongoing.
     * The lock will be released as soon as the transaction will be committed or rolled back,
     * or if the connection is lost.
     * <p>
     * Using a lock on the data store can be useful later on if we wish to externalize the store and
     * scale out the java servers - We will have a central lock, shared between the java instances.
     *
     * @return The read account
     * @throws SQLException In case the operation against the data store fails
     */
    @NotNull
    private Account loadAndLockAccount(@NotNull UUID accountId, @NotNull Connection connection) throws SQLException {
        logger.debug("Load and lock account: {}", accountId);
        var loadForUpdateQuery = "SELECT id, balance, owner FROM " + ACCOUNT_TABLE + " WHERE id = ? FOR UPDATE";
        var loaded = runner.query(connection, loadForUpdateQuery, accountHandler, accountId);
        if (loaded == null) {
            throw new UnknownAccountException(accountId);
        }
        return loaded;
    }
}

