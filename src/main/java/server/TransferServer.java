package server;

import account.AccountItemsFactory;
import account.AccountManager;
import account.AccountManagerImpl;
import com.google.gson.Gson;
import datastore.SQLDataStoreInMemoryH2;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reporting.AccountManagementException;
import reporting.OperationException;
import spark.Request;
import transaction.TransactionFactory;
import transaction.TxManager;
import transaction.TxManagerDataStoreImpl;

import java.io.File;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static reporting.TransactionStatus.INVALID_USER_ID;
import static spark.Spark.*;

public class TransferServer {

    private static final Logger logger = LoggerFactory.getLogger(TransferServer.class);

    @NotNull
    private final SQLDataStoreInMemoryH2 dataStore = SQLDataStoreInMemoryH2.INSTANCE;

    @NotNull
    private final AccountManager accountManager = AccountManagerImpl.INSTANCE;

    @NotNull
    private final TxManager txManager = TxManagerDataStoreImpl.INSTANCE;

    @NotNull
    private final Gson gson = new Gson();

    @NotNull
    private final AtomicInteger requestId = new AtomicInteger(1);

    public static void main(String[] args) {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("system.properties"));
            new TransferServer(
                    config.getString("server.listens"),
                    config.getInt("server.port"),
                    config.getInt("server.threads")
            );
        } catch (ConfigurationException ex) {
            logger.error("Failed to load the server configuration", ex);
            throw new ServerSystemException();
        }
    }

    TransferServer(@NotNull String ipAddress, int port, int maxThreads) {
        logger.info("Starting up application server");
        configureServer(ipAddress, port, maxThreads);
        setupRouting();
        setupExceptionHandling();
        setupBeforeAndAfter();
        logger.info("Server has started");
    }

    private void setupRouting() {

        get("/status", (request, response) -> gson.toJson(Map.of("status", "up")));

        path("/transaction", () -> {
            get("/to/:account", (request, response) -> {
                var accountId = AccountItemsFactory.buildAccountIdFromString(request.params("account"));
                return gson.toJson(txManager.loadTransactionsTo(accountId, userId(request)));
            });
            get("/from/:account", (request, response) -> {
                var accountId = AccountItemsFactory.buildAccountIdFromString(request.params("account"));
                return gson.toJson(txManager.loadTransactionsFrom(accountId, userId(request)));
            });
            post("/:from/:to/:amount", (request, response) -> {
                var orderTx = TransactionFactory.initTransactionFromUserInput(
                        request.params("to"),
                        request.params("from"),
                        request.params("amount")
                );
                var doneTx = txManager.executeTransaction(orderTx, userId(request));
                return gson.toJson(doneTx);
            });
        });

        path("/account", () -> {
            put("/:initialBalance", (request, response) -> {
                var account = accountManager.createAccount(request.params("initialBalance"), userId(request));
                return gson.toJson(account);
            });
            get("/:accountId", (request, response) -> {
                var account = accountManager.loadAccount(request.params("accountId"), userId(request));
                return gson.toJson(account);
            });
        });
    }

    private void setupBeforeAndAfter() {

        before((request, response) -> {
            response.type("application/json");
            request.attribute("requestId", requestId.getAndIncrement());
            logger.info(">>> {} >>> Start of a {} request was made on {}",
                    request.attribute("requestId"),
                    request.requestMethod(),
                    request.pathInfo());
        });

        after((request, response) ->
                logger.info("<<< {} <<< HTTP:200 {} on {}",
                        request.attribute("requestId"),
                        request.requestMethod(),
                        request.pathInfo())
        );
    }

    private void setupExceptionHandling() {
        exception(OperationException.class, (exception, request, response) -> {
            logger.warn("A new operation exception was thrown " + exception.getMessage(), exception);
            logger.info("<<< {} <<< HTTP:{} {} on {}",
                    request.attribute("requestId"),
                    exception.getHttpCode(),
                    request.requestMethod(),
                    request.pathInfo());
            var error = new ErrorResponse(exception);
            response.body(gson.toJson(error));
            response.status(exception.getHttpCode());
        });

        exception(Exception.class, (exception, request, response) -> {
            logger.warn("An unexpected exception was thrown: " + exception.getMessage(), exception);
            var error = new ErrorResponse(exception.getMessage(), "SYSTEM");
            logger.info("<<< {} <<< HTTP:{} {} on {}",
                    request.attribute("requestId"),
                    500,
                    request.requestMethod(),
                    request.pathInfo());
            response.body(gson.toJson(error));
            response.status(500);
        });

        notFound((req, res) -> {
            var error = new ErrorResponse("The route you are querying does not exist", "ROUTING");
            return gson.toJson(error);
        });
    }

    private void configureServer(@NotNull String ipAddress, int port, int maxThreads) {
        logger.info("Configuration is: ip = {}, port = {}, max thread = {}", ipAddress, port, maxThreads);
        port(port);
        ipAddress(ipAddress);
        threadPool(maxThreads);
        dataStore.setMaxConnections(maxThreads);
    }

    private int userId(@NotNull Request request) {
        try {
            var uid = Integer.parseInt(request.headers("uid"));
            if (uid < 0) {
                throw new AccountManagementException(INVALID_USER_ID);
            }
            return uid;
        } catch (NumberFormatException e) {
            throw new AccountManagementException(INVALID_USER_ID);
        }
    }
}
