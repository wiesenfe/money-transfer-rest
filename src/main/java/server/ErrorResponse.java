package server;

import org.jetbrains.annotations.NotNull;
import reporting.OperationException;

final class ErrorResponse {

    @NotNull
    @SuppressWarnings("unused")
    private final String errorMessage;

    @NotNull
    @SuppressWarnings("unused")
    private final String errorCode;

    ErrorResponse(@NotNull String message, @NotNull String errorCode) {
        this.errorMessage = message;
        this.errorCode = errorCode;
    }

    ErrorResponse(@NotNull OperationException exception) {
        this.errorMessage = exception.getMessage();
        this.errorCode = exception.getStatus().toString();
    }
}
