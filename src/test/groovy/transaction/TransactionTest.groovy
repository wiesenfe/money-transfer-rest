package transaction


import spock.lang.Shared
import spock.lang.Specification

import static reporting.TransactionStatus.STARTED

class TransactionTest extends Specification {

    @Shared
    def idOne = UUID.fromString("00000000-0000-0000-0000-000000000000")
    @Shared
    def idTwo = UUID.fromString("00000000-0000-0000-0000-000000000001")
    @Shared
    def idThree = UUID.fromString("00000000-0000-0000-0000-000000000002")


    def "Should know if the two accounts of the transactions are identical"() {
        when:
        def tx1 = new Transaction(UUID.randomUUID(), 0L, idOne, idTwo, new BigDecimal("1000"), STARTED)
        def tx2 = new Transaction(UUID.randomUUID(), 0L, idThree, idThree, new BigDecimal("1000"), STARTED)
        then:
        !tx1.accountsAreIdentical()
        tx2.accountsAreIdentical()
    }

    def "Should be able to tell if the first account (sorting by id) is the account to debit"() {
        when:
        def tx1 = new Transaction(UUID.randomUUID(), 0L, idOne, idTwo, new BigDecimal("1000"), STARTED)
        def tx2 = new Transaction(UUID.randomUUID(), 0L, idTwo, idOne, new BigDecimal("1000"), STARTED)
        def tx3 = new Transaction(UUID.randomUUID(), 0L, idThree, idThree, new BigDecimal("1000"), STARTED)
        then:
        !tx1.firstAccountToDebit
        tx2.firstAccountToDebit
        tx3.firstAccountToDebit
    }

    def "Should return the account with the smallest UUID"() {
        when:
        def tx1 = new Transaction(UUID.randomUUID(), 0L, idOne, idTwo, new BigDecimal("1000"), STARTED)
        def tx2 = new Transaction(UUID.randomUUID(), 0L, idThree, idTwo, new BigDecimal("1000"), STARTED)
        def tx3 = new Transaction(UUID.randomUUID(), 0L, idThree, idThree, new BigDecimal("1000"), STARTED)
        then:
        tx1.firstAccountId == idOne
        tx2.firstAccountId == idTwo
        tx3.firstAccountId == idThree
    }

    def "Should return the account with the largest UUID"() {
        when:
        def tx1 = new Transaction(UUID.randomUUID(), 0L, idOne, idTwo, new BigDecimal("1000"), STARTED)
        def tx2 = new Transaction(UUID.randomUUID(), 0L, idThree, idTwo, new BigDecimal("1000"), STARTED)
        def tx3 = new Transaction(UUID.randomUUID(), 0L, idThree, idThree, new BigDecimal("1000"), STARTED)
        then:
        tx1.secondAccountId == idTwo
        tx2.secondAccountId == idThree
        tx3.secondAccountId == idThree
    }
}
