package transaction

import reporting.AccountManagementException
import reporting.TransactionStatus
import spock.lang.Specification

class TransactionFactoryTest extends Specification {
    def "init transaction from sane user input"() {
        when:
        def id1 = UUID.randomUUID()
        def id2 = UUID.randomUUID()
        def tx = TransactionFactory.initTransactionFromUserInput(id1.toString(), id2.toString(), "100.0")

        then:
        with(tx) {
            status == TransactionStatus.STARTED
            accountIdToCredit == id1
            accountIdToDebit == id2
            amount == new BigDecimal("100")
        }
    }

    def "init transaction from malformed user input: null UUID"() {
        when:
        TransactionFactory.initTransactionFromUserInput(null, UUID.randomUUID().toString(), "100.0")

        then:
        thrown(AccountManagementException)
    }

    def "init transaction from malformed user input: null amount"() {
        when:
        TransactionFactory.initTransactionFromUserInput(UUID.randomUUID().toString(), UUID.randomUUID().toString(), null)

        then:
        thrown(AccountManagementException)
    }
}
