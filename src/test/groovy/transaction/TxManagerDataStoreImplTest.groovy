package transaction

import account.AccountHandler
import datastore.SQLDataStoreInMemoryH2
import org.apache.commons.dbutils.QueryRunner
import spock.lang.Shared
import spock.lang.Specification

import static datastore.SQLDataStore.ACCOUNT_TABLE
import static datastore.SQLDataStore.TX_TABLE
import static reporting.TransactionStatus.*
import static transaction.TransactionFactory.initTransaction

class TxManagerDataStoreImplTest extends Specification {

    def transactionManager = TxManagerDataStoreImpl.INSTANCE

    @Shared
    def idOne = UUID.randomUUID()
    @Shared
    def idTwo = UUID.randomUUID()
    @Shared
    def idThree = UUID.randomUUID()
    @Shared
    def idFour = UUID.randomUUID()

    def setup() {
        def insertTableQuery = "INSERT INTO " + ACCOUNT_TABLE + " VALUES ( ? , ? , ? )"
        def initialBalance = new BigDecimal("1000.00")
        def con = SQLDataStoreInMemoryH2.INSTANCE.connection
        def runner = new QueryRunner()

        runner.execute(con, "TRUNCATE TABLE " + TX_TABLE)
        runner.execute(con, "TRUNCATE TABLE " + ACCOUNT_TABLE)
        runner.execute(con, insertTableQuery, idOne, initialBalance, 1)
        runner.execute(con, insertTableQuery, idTwo, initialBalance, 2)
        runner.execute(con, insertTableQuery, idThree, initialBalance, 3)
        con.commit()
        con.close()
    }

    def loadOneAccount(UUID accountId) {
        def loadQuery = "SELECT id, balance, owner FROM " + ACCOUNT_TABLE + " WHERE id = ?"
        return SQLDataStoreInMemoryH2.INSTANCE.readOnlyQuery(loadQuery, new AccountHandler(), accountId)
    }

    def "Execute transaction in a nominal case. Send 100.00 units from 2 to 1"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, new BigDecimal("100.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == SUCCESS

        and:
        transactionManager.countTransactions() == 1

        and:
        loadOneAccount(idOne).balance == new BigDecimal("1100.00")
        loadOneAccount(idTwo).balance == new BigDecimal("900.00")
        loadOneAccount(idThree).balance == new BigDecimal("1000.00")
    }

    def "Execute transaction in a nominal case. Empties account 2 into 1"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, new BigDecimal("1000.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == SUCCESS

        and:
        transactionManager.countTransactions() == 1
        transactionManager.loadTransactionsFrom(idTwo, 2)[0].amount == new BigDecimal("1000.00")

        and:
        loadOneAccount(idOne).balance == new BigDecimal("2000.00")
        loadOneAccount(idTwo).balance == new BigDecimal("0.00")
        loadOneAccount(idThree).balance == new BigDecimal("1000.00")
    }

    def "Execute transaction when the user does not own the debited account"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, new BigDecimal("10.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 1)

        then:
        executedTx.getStatus() == USER_NOT_OWNER

        and:
        loadOneAccount(idOne).balance == new BigDecimal("1000.00")
        loadOneAccount(idTwo).balance == new BigDecimal("1000.00")

    }

    def "Execute a cycle of transactions in a nominal case."() {
        when:
        [
                [initTransaction(idOne, idTwo, new BigDecimal("100.00")), 2], // Two: 900, One: 1100
                [initTransaction(idTwo, idThree, new BigDecimal("200.00")), 3], // Three: 800, Two: 1100
                [initTransaction(idThree, idTwo, new BigDecimal("300.00")), 2], // Two: 800, Three: 1100
                [initTransaction(idTwo, idOne, new BigDecimal("400.00")), 1], // One: 700, Two: 1200
        ].each { transactionManager.executeTransaction((Transaction) it[0], (Integer) it[1]) }

        then:
        transactionManager.countTransactions() == 4

        and:
        transactionManager.loadTransactionsFrom(idOne, 1).size() == 1
        transactionManager.loadTransactionsTo(idOne, 1).size() == 1
        transactionManager.loadTransactionsFrom(idOne, 1)[0].amount == new BigDecimal("400.00")
        transactionManager.loadTransactionsTo(idOne, 1)[0].amount == new BigDecimal("100.00")

        and:
        loadOneAccount(idOne).balance == new BigDecimal("700.00")
        loadOneAccount(idTwo).balance == new BigDecimal("1200.00")
        loadOneAccount(idThree).balance == new BigDecimal("1100.00")
    }

    def "Execute transaction with a negative amount"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, new BigDecimal("-100.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == NEGATIVE_AMOUNT_IN_TRANSFER
        transactionManager.countTransactions() == 0
    }

    def "Execute transaction with a no fund"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, BigDecimal.ZERO)
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == NO_FUND_TO_TRANSFER
        transactionManager.countTransactions() == 0
    }

    def "Execute transaction to the same account"() {
        when:
        def transactionToExecute = initTransaction(idOne, UUID.fromString(idOne.toString()), new BigDecimal("100.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 1)

        then:
        executedTx.getStatus() == SAME_ACCOUNT_TRANSFER
        transactionManager.countTransactions() == 0
    }

    def "Tries to send units from an unknown account"() {
        when:
        def transactionToExecute = initTransaction(idOne, idFour, new BigDecimal("100.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 4)

        then:
        executedTx.getStatus() == UNKNOWN_SOURCE
        transactionManager.countTransactions() == 0
        [idOne, idTwo, idThree].each { loadOneAccount(it).balance == new BigDecimal("1000.00") }
    }

    def "Tries to send units to an unknown account"() {
        when:
        def transactionToExecute = initTransaction(idFour, idTwo, new BigDecimal("100.00"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == UNKNOWN_BENEFICIARY
        transactionManager.countTransactions() == 0
        [idOne, idTwo, idThree].each { loadOneAccount(it).balance == new BigDecimal("1000.00") }
    }

    def "Tries to send units when there are not enough funds on the source account"() {
        when:
        def transactionToExecute = initTransaction(idOne, idTwo, new BigDecimal("1000.01"))
        def executedTx = transactionManager.executeTransaction(transactionToExecute, 2)

        then:
        executedTx.getStatus() == NOT_ENOUGH_FUND
        transactionManager.countTransactions() == 0
        [idOne, idTwo, idThree].each { loadOneAccount(it).balance == new BigDecimal("1000.00") }
    }
}
