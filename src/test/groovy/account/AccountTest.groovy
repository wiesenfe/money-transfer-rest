package account

import spock.lang.Specification

class AccountTest extends Specification {

    def "should create a copy of the account with the retrieved amount"() {
        when:
        def id = UUID.randomUUID()
        def account = new Account(id, BigDecimal.TEN, 1).retrieveFunds(BigDecimal.ONE)
        then:
        with(account) {
            id == id
            balance == BigDecimal.valueOf(9)
        }
    }

    def "should create a copy of the account with the same amount when retrieving 0"() {
        when:
        def id = UUID.randomUUID()
        def account = new Account(id, BigDecimal.TEN, 1).retrieveFunds(BigDecimal.ZERO)
        then:
        with(account) {
            id == id
            balance == BigDecimal.TEN
        }
    }

    def "should throw an IllegalArgumentException when trying to retrieve negative amounts"() {
        when:
        new Account(UUID.randomUUID(), BigDecimal.TEN, 1).retrieveFunds(BigDecimal.valueOf(-9))
        then:
        thrown(IllegalArgumentException)
    }

    def "should create a copy of the account with the deposited amount"() {
        when:
        def id = UUID.randomUUID()
        def account = new Account(id, BigDecimal.TEN, 1).depositFunds(BigDecimal.ONE)
        then:
        with(account) {
            id == id
            balance == BigDecimal.valueOf(11)
        }
    }

    def "should create a copy of the account with the same amount when making a deposit of 0"() {
        when:
        def id = UUID.randomUUID()
        def account = new Account(id, BigDecimal.TEN, 1).depositFunds(BigDecimal.ZERO)
        then:
        with(account) {
            id == id
            balance == BigDecimal.TEN
        }
    }

    def "should throw an IllegalArgumentException when trying to deposit a negative amounts"() {
        when:
        new Account(UUID.randomUUID(), BigDecimal.TEN, 1).depositFunds(BigDecimal.valueOf(-9))
        then:
        thrown(IllegalArgumentException)
    }

    def "should have sufficient fund"() {
        when:
        def account = new Account(UUID.randomUUID(), BigDecimal.TEN, 1)
        then:
        account.hasSufficientFund(BigDecimal.ONE)
        account.hasSufficientFund(BigDecimal.ZERO)
        account.hasSufficientFund(BigDecimal.TEN)
    }

    def "should not have sufficient fund"() {
        when:
        def account = new Account(UUID.randomUUID(), BigDecimal.ONE, 1)
        then:
        !account.hasSufficientFund(BigDecimal.TEN)
    }

    def "should throw an exception when testing sufficient funds against a negative number"() {
        when:
        new Account(UUID.randomUUID(), BigDecimal.ONE, 1).hasSufficientFund(BigDecimal.valueOf(-9))
        then:
        thrown(IllegalArgumentException)
    }

    def "should properly deny / grant access"() {
        when:
        def account = new Account(UUID.randomUUID(), BigDecimal.ONE, 1)
        then:
        !account.canBeAccessedBy(2)
        account.canBeAccessedBy(1)
    }
}
