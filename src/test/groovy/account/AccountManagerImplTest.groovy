package account

import datastore.SQLDataStoreInMemoryH2
import org.apache.commons.dbutils.QueryRunner
import reporting.AccountManagementException
import spock.lang.Specification

import static datastore.SQLDataStore.ACCOUNT_TABLE
import static reporting.TransactionStatus.*

class AccountManagerImplTest extends Specification {

    def setup() {
        def con = SQLDataStoreInMemoryH2.INSTANCE.connection
        def runner = new QueryRunner()
        runner.execute(con, "TRUNCATE TABLE " + ACCOUNT_TABLE)
        con.commit()
        con.close()
    }

    def "creates an account and checks that it is correctly persisted in the database"() {
        when:
        def account = AccountManagerImpl.INSTANCE.createAccount("100.00", 1)
        def accountFromDb = SQLDataStoreInMemoryH2.INSTANCE.readOnlyQuery(
                "SELECT * FROM " + ACCOUNT_TABLE + " WHERE id = ?",
                new AccountHandler(),
                account.id)

        then:
        account.balance == BigDecimal.valueOf(100)

        and:
        accountFromDb.owner == 1
        with(accountFromDb) {
            balance == BigDecimal.valueOf(100)
            id == account.id
        }
    }

    def "creates an account with no initial fund and checks that it is correctly persisted in the database"() {
        when:
        def account = AccountManagerImpl.INSTANCE.createAccount("0", 1)
        def accountFromDb = SQLDataStoreInMemoryH2.INSTANCE.readOnlyQuery(
                "SELECT * FROM " + ACCOUNT_TABLE + " WHERE id = ?",
                new AccountHandler(),
                account.id)

        then:
        account.balance == BigDecimal.ZERO
        accountFromDb.balance == BigDecimal.ZERO
        accountFromDb.id == account.id
        accountFromDb.owner == 1
    }

    def "creates an account with a negative amount and checks that an error is raised"() {
        when:
        AccountManagerImpl.INSTANCE.createAccount("-100", 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == AMOUNT_IS_NEGATIVE
    }

    def "creates an account with a null amount and checks that an error is raised"() {
        when:
        AccountManagerImpl.INSTANCE.createAccount(null, 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == ACCOUNT_WITH_MALFORMED_INITIAL_BALANCE
    }

    def "creates an account with a malformed amount and checks that an error is raised"() {
        when:
        AccountManagerImpl.INSTANCE.createAccount("a123f- -", 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == ACCOUNT_WITH_MALFORMED_INITIAL_BALANCE
    }

    def "Checks that accounts are loaded correctly"() {
        setup:
        def idOne = UUID.randomUUID()
        def idTwo = UUID.randomUUID()
        def idThree = UUID.randomUUID()
        def insertTableQuery = "INSERT INTO " + ACCOUNT_TABLE + " VALUES ( ? , ? , ? )"
        def runner = new QueryRunner()
        def con = SQLDataStoreInMemoryH2.INSTANCE.connection

        when:
        runner.execute(con, insertTableQuery, idOne, BigDecimal.ZERO, 1)
        runner.execute(con, insertTableQuery, idTwo, BigDecimal.ONE, 2)
        runner.execute(con, insertTableQuery, idThree, BigDecimal.TEN, 3)
        con.commit()

        then:
        def loaded1 = AccountManagerImpl.INSTANCE.loadAccount(idOne.toString(), 1)
        def loaded2 = AccountManagerImpl.INSTANCE.loadAccount(idTwo.toString(), 2)
        def loaded3 = AccountManagerImpl.INSTANCE.loadAccount(idThree.toString(), 3)

        loaded1.owner == 1
        loaded2.owner == 2
        loaded3.owner == 3

        with(loaded1) {
            id == idOne
            balance == BigDecimal.ZERO
        }
        with(loaded2) {
            id == idTwo
            balance == BigDecimal.ONE
        }
        with(loaded3) {
            id == idThree
            balance == BigDecimal.TEN
        }

        cleanup:
        con.close()
    }

    def "Checks that access is granted only to the account's administrator"() {
        setup:
        def idOne = UUID.randomUUID()
        def insertTableQuery = "INSERT INTO " + ACCOUNT_TABLE + " VALUES ( ? , ? , ? )"
        def runner = new QueryRunner()
        def con = SQLDataStoreInMemoryH2.INSTANCE.connection

        when:
        runner.execute(con, insertTableQuery, idOne, BigDecimal.ZERO, 1)
        con.commit()
        AccountManagerImpl.INSTANCE.loadAccount(idOne.toString(), 2)

        then:
        def e = thrown(AccountManagementException)
        e.status == USER_NOT_OWNER

        cleanup:
        con.close()
    }

    def "Checks that an unknown account is read as null"() {
        when:
        AccountManagerImpl.INSTANCE.loadAccount(UUID.randomUUID().toString(), 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == ACCOUNT_IS_UNKNOWN
    }

    def "Checks that a malformed uuid leads to an exception"() {
        when:
        AccountManagerImpl.INSTANCE.loadAccount("foo", 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == INVALID_UUID
    }

    def "Checks that a null uuid leads to an exception"() {
        when:
        AccountManagerImpl.INSTANCE.loadAccount(null, 1)

        then:
        def e = thrown(AccountManagementException)
        e.status == INVALID_UUID
    }
}
