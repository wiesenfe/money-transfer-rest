package server

import groovyx.net.http.HttpResponseException
import spock.lang.Specification
import spock.lang.Unroll

import static server.TestServer.apiClient

class SingleThreadedFunctionalTest extends Specification {

    @SuppressWarnings("all")
    def setupSpec() {
        TestServer.instance
    }

    def setup() {
        TestServer.resetDataStore()
    }

    def "Server should answer up to a status request"() {
        when:
        def response = apiClient(1).get(path: "/status")

        then:
        response['responseData']['status'] == "up"
    }

    @Unroll
    def "Users should be able to create a few accounts with initial balance of their choice"(double initialAmount,
                                                                                             int userId) {
        setup:
        def response = apiClient(userId).put(path: "/account/${initialAmount}")

        when:
        def createdAccountId = response['responseData']['id']
        def balanceResponse = apiClient(userId).get(path: "/account/${createdAccountId}")

        then:
        balanceResponse['responseData']['balance'] == initialAmount
        balanceResponse['status'] == 200

        where:
        initialAmount | userId
        100.0         | 1
        658.0         | 2
        000.0         | 3
        010.0         | 2
        0.123         | 1
        1231314431.45 | 1234
    }

    def "User should not be able to access an account he did not create"() {
        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        apiClient(2).get(path: "/account/${account1}")['responseData']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 401
    }

    def "User should not be able to make any transactions from an account he did not create"() {
        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = apiClient(2).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        def tx1 = apiClient(2).post(path: "/transaction/${account1}/${account2}/10")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']
        def account2After = apiClient(2).get(path: "/account/${account2}")['responseData']

        then:
        tx1['status'] == "USER_NOT_OWNER"
        account1After['balance'] == initialAmount
        account2After['balance'] == initialAmount
    }

    def "User should be able to load transactions from an account he did not create"() {
        setup:
        def account1 = apiClient(1).put(path: "/account/0")['responseData']['id']

        when:
        apiClient(3).get(path: "/transaction/from/${account1}")['responseData']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 401
    }

    def "User should be able to load transactions to an account he did not create"() {
        setup:
        def account1 = apiClient(1).put(path: "/account/0")['responseData']['id']

        when:
        apiClient(3).get(path: "/transaction/to/${account1}")['responseData']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 401
    }

    def "User should be able to create accounts and make transactions between them"() {
        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = apiClient(2).put(path: "/account/${initialAmount}")['responseData']['id']
        def account3 = apiClient(3).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        def tx1 = apiClient(1).post(path: "/transaction/${account1}/${account2}/10")['responseData']
        def tx2 = apiClient(2).post(path: "/transaction/${account2}/${account3}/20")['responseData']
        def tx3 = apiClient(3).post(path: "/transaction/${account3}/${account1}/30")['responseData']

        and:
        def txTo1 = apiClient(1).get(path: "/transaction/to/${account1}")['responseData']
        def txTo2 = apiClient(2).get(path: "/transaction/to/${account2}")['responseData']
        def txTo3 = apiClient(3).get(path: "/transaction/to/${account3}")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']
        def account2After = apiClient(2).get(path: "/account/${account2}")['responseData']
        def account3After = apiClient(3).get(path: "/account/${account3}")['responseData']

        then:
        tx1['status'] == "SUCCESS"
        tx2['status'] == "SUCCESS"
        tx3['status'] == "SUCCESS"

        and:
        account1After['balance'] == 120
        account2After['balance'] == 90
        account3After['balance'] == 90

        and:
        (txTo1 as List).get(0)['amount'] == 30
        (txTo3 as List).get(0)['amount'] == 20
        (txTo2 as List).get(0)['amount'] == 10
    }

    def "User should not be able to create account with negative amount of money"() {
        when:
        def initialAmount = -100
        apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 400
    }

    def "User should not be able to create account with non numeric amount of money"() {
        when:
        def initialAmount = "abc"
        apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 400
    }

    def "User should not be able to create account without a valid uid"() {
        when:
        def initialAmount = 100
        apiClient(-1).put(path: "/account/${initialAmount}")['responseData']['id']

        then:
        def e = thrown(HttpResponseException)
        e.statusCode == 401
    }

    def "User should not be allowed to send more money than he has"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = apiClient(2).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        def tx1 = apiClient(1).post(path: "/transaction/${account1}/${account2}/50")['responseData']
        def tx2 = apiClient(1).post(path: "/transaction/${account1}/${account2}/60")['responseData']

        and:
        def txFrom1 = apiClient(1).get(path: "/transaction/from/${account1}")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']
        def account2After = apiClient(2).get(path: "/account/${account2}")['responseData']

        then:
        tx1['status'] == "SUCCESS"
        tx2['status'] == "NOT_ENOUGH_FUND"

        and:
        account1After['balance'] == 50
        account2After['balance'] == 150

        and:
        (txFrom1 as List).size() == 1
        (txFrom1 as List).get(0)['amount'] == 50
    }

    def "User should not be able to send money to himself"() {
        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        def tx1 = apiClient(1).post(path: "/transaction/${account1}/${account1}/50")['responseData']

        and:
        def txFrom1 = apiClient(1).get(path: "/transaction/from/${account1}")['responseData']
        def txTo1 = apiClient(1).get(path: "/transaction/to/${account1}")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']

        then:
        tx1['status'] == "SAME_ACCOUNT_TRANSFER"

        and:
        account1After['balance'] == 100

        and:
        (txFrom1 as Collection).size() == 0
        (txTo1 as Collection).size() == 0
    }

    def "User should not be able to send negative amounts of money"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = apiClient(2).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        apiClient(1).post(path: "/transaction/${account1}/${account2}/-50")

        then:
        thrown(HttpResponseException)

        and:
        def txFrom1 = apiClient(1).get(path: "/transaction/from/${account1}")['responseData']
        def txTo2 = apiClient(2).get(path: "/transaction/to/${account2}")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']
        def account2After = apiClient(2).get(path: "/account/${account2}")['responseData']

        and:
        account1After['balance'] == 100
        account2After['balance'] == 100

        and:
        (txFrom1 as Collection).size() == 0
        (txTo2 as Collection).size() == 0
    }

    def "User should not be able to send 0 credit to someone else"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = apiClient(2).put(path: "/account/${initialAmount}")['responseData']['id']

        when:
        def tx = apiClient(1).post(path: "/transaction/${account1}/${account2}/000")['responseData']

        then:
        tx['status'] == "NO_FUND_TO_TRANSFER"

        and:
        def txFrom1 = apiClient(1).get(path: "/transaction/from/${account1}")['responseData']
        def txTo2 = apiClient(2).get(path: "/transaction/to/${account2}")['responseData']

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']
        def account2After = apiClient(2).get(path: "/account/${account2}")['responseData']

        and:
        account1After['balance'] == 100
        account2After['balance'] == 100

        and:
        (txFrom1 as Collection).size() == 0
        (txTo2 as Collection).size() == 0
    }

    def "User should not be able to send money to malformed accounts"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = UUID.randomUUID().toString() + "0"

        when:
        apiClient(1).post(path: "/transaction/${account1}/${account2}/50")

        then:
        thrown(HttpResponseException)
    }


    def "User should not be able to send money to unknown accounts"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = UUID.randomUUID()

        when:
        def tx = apiClient(1).post(path: "/transaction/${account1}/${account2}/50")['responseData']

        then:
        tx['status'] == "UNKNOWN_BENEFICIARY"

        and:
        def txFrom1 = apiClient(1).get(path: "/transaction/from/${account1}")['responseData']
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']

        and:
        account1After['balance'] == 100
        (txFrom1 as Collection).size() == 0
    }

    def "User should not be able to send money from unknown accounts"() {

        setup:
        def initialAmount = 100
        def account1 = apiClient(1).put(path: "/account/${initialAmount}")['responseData']['id']
        def account2 = UUID.randomUUID()

        when:
        def tx = apiClient(1).post(path: "/transaction/${account2}/${account1}/50")['responseData']

        then:
        tx['status'] == "UNKNOWN_SOURCE"

        and:
        def account1After = apiClient(1).get(path: "/account/${account1}")['responseData']

        and:
        account1After['balance'] == 100
    }
}
