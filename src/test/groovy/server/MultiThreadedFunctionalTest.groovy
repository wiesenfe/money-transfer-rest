package server

import account.Account
import spock.lang.Specification
import spock.lang.Unroll

import static server.TestServer.apiClient

class MultiThreadedFunctionalTest extends Specification {

    def setupSpec() {
        TestServer.instance
    }

    def setup() {
        TestServer.resetDataStore()
    }

    @Unroll
    def "N providers should be able to make  concurrent transactions on J accounts"(
            int nbThreads, int nbTransactions, int nbAccounts) {

        setup:
        def initialAmount = 10000
        def connection = apiClient(1)
        def accounts = (1..nbAccounts).collect { connection.put(path: "/account/${initialAmount}")['responseData'].id }

        Collection<Thread> threads = []
        nbThreads.times {
            def provider = new Provider(accounts, nbTransactions)
            threads.add(new Thread(provider))
        }

        when:
        threads.each { (it as Thread).start() }
        threads.each { (it as Thread).join() }

        and:
        def txToAccounts = accounts.collect { connection.get(path: "/transaction/to/${it}")['responseData'] }
        def txFromAccounts = accounts.collect { connection.get(path: "/transaction/from/${it}")['responseData'] }
        def accountsAfter = accounts.collect { connection.get(path: "/account/${it}")['responseData'] }

        then:
        // Check that the global amount of money was preserved
        (accountsAfter as List<Account>)*.balance.sum() == nbAccounts * initialAmount

        and:
        // Checks that each account is coherent with the transactions
        for (def i = 0; i < nbAccounts; i++) {
            def moneySentFromI = txFromAccounts[i].collect { it['amount'] }.sum() ?: 0
            def moneySentToI = txToAccounts[i].collect { it['amount'] }.sum() ?: 0
            initialAmount - (moneySentFromI as Integer) + (moneySentToI as Integer) == accountsAfter[i]['balance']
        }

        and:
        // Check the total number of transactions
        def totalTxToAccounts = (txToAccounts as Collection<Collection>)*.size().sum()
        def totalTxFromAccounts = (txFromAccounts as Collection<Collection>)*.size().sum()
        totalTxFromAccounts == totalTxToAccounts
        totalTxFromAccounts == nbThreads * nbTransactions

        cleanup:
        connection.shutdown()

        where:
        nbThreads | nbTransactions | nbAccounts
        3         | 1000           | 100
        3         | 1000           | 2
        20        | 1000           | 100
        20        | 1000           | 2
        100       | 100            | 2

    }

    /**
     * Simulates a provider that executes
     * transactions on behalf of a list of bank accounts.
     */
    class Provider implements Runnable {

        final rand = new Random()
        final List bankAccounts
        final int nbTransactions
        final client


        Provider(bankAccounts = [], nbTransaction) {
            this.bankAccounts = bankAccounts
            this.nbTransactions = nbTransaction
            client = apiClient(1)
        }

        @Override
        void run() {
            nbTransactions.times {
                def amount = 1 + rand.nextInt(10)
                def id1 = rand.nextInt(bankAccounts.size())
                def id2 = rand.nextInt(bankAccounts.size())

                if (id1 == id2) { // Avoids same accounts transfers
                    id1 = (id1 + 1 < bankAccounts.size()) ? id1 + 1 : id1 - 1
                }

                def account1 = bankAccounts.get(id1)
                def account2 = bankAccounts.get(id2)

                try {
                    this.client.post(path: "/transaction/${account1}/${account2}/${amount}")
                }
                finally {
                    sleep(rand.nextInt(10))
                }
            }
        }
    }
}
