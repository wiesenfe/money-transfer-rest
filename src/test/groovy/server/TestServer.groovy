package server

import datastore.SQLDataStoreInMemoryH2
import groovyx.net.http.RESTClient
import org.apache.commons.dbutils.QueryRunner

import static datastore.SQLDataStore.ACCOUNT_TABLE
import static datastore.SQLDataStore.TX_TABLE

final class TestServer {

    private static ipAddress = "127.0.0.1"
    private static port = 4242
    private static server

    static synchronized getInstance() {
        if (server == null) {
            server = new TestServer()
        }
        return server
    }

    static apiClient(int userId = 0) {
        def client = new RESTClient("http://${ipAddress}:${port}")
        client.setHeaders(Map.of("uid", String.valueOf(userId)))
        return client
    }

    static resetDataStore() {
        def con = SQLDataStoreInMemoryH2.INSTANCE.connection
        def runner = new QueryRunner()
        runner.execute(con, "TRUNCATE TABLE " + TX_TABLE)
        runner.execute(con, "TRUNCATE TABLE " + ACCOUNT_TABLE)
        con.commit()
        con.close()
    }

    private TestServer() {
        initServer()
    }

    @SuppressWarnings("all")
    private initServer() {
        def serverIsUp = false
        def server = new TransferServer(ipAddress, port, 100)
        for (it in (1..60)) {
            try {
                apiClient().get(path: "/status")
                serverIsUp = true
                break
            } catch (e) {
                sleep(500)
            }
        }
        if (!serverIsUp) {
            throw new IllegalStateException("The server could not be started")
        }
    }
}
